<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <form method="post">
            <input type="text" name="email" placeholder="">
            <input type="submit" value="Buscar">
        </form>
        <?php if (sizeof($empleados) > 0): ?>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Position</th>
                    <th>Salary</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($empleados as $empleado): ?>
                <tr>
                    <td><?= $empleado['name'] ?></td>
                    <td><?= $empleado['email'] ?></td>
                    <td><?= $empleado['position'] ?></td>
                    <td><?= $empleado['salary'] ?></td>
                    <td><a href="empleado/<?= $empleado['id'] ?>">Ver</a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php else: ?>
            <p>No se encontraron resultados</p>
        <?php endif; ?>
    </body>
</html>