<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <table>
        <tr>
            <th>Name:</th>
            <td><?= $empleado['name'] ?></td>
        </tr>
        <tr>
            <th>Email:</th>
            <td><?= $empleado['email'] ?></td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td><?= $empleado['phone'] ?></td>
        </tr>
        <tr>
            <th>Address:</th>
            <td><?= $empleado['address'] ?></td>
        </tr>
        <tr>
            <th>Position:</th>
            <td><?= $empleado['position'] ?></td>
        </tr>
        <tr>
            <th>Salary:</th>
            <td><?= $empleado['salary'] ?></td>
        </tr>
        <tr>
            <th>Skills:</th>
            <td>
                <ul>
                <?php foreach ($empleado['skills'] as $skill): ?>
                <li><?= $skill['skill'] ?></li>
                <?php endforeach; ?>
                </ul>
            </td>
        </tr>
    </table>
</body>
</html>