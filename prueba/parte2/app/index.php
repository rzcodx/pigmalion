<?php

include "vendor/autoload.php";

$app = new \Slim\App();

require "model/empleado_model.php";
require "routes/empleado_route.php";

$app->run();