<?php namespace Model;

class Empleado
{
    public function getAll()
    {
        $data = file_get_contents("employees.json");
        $empleados = json_decode($data, true);
        return $empleados;
    }

    public function buscar($email)
    {
        $data = file_get_contents("employees.json");
        $_empleados = json_decode($data, true);
        $empleados = [];

        if ($email != "")
        {
            foreach ($_empleados as $empleado)
            {
                if (strpos($empleado['email'], $email) !== false)
                {
                    array_push($empleados, $empleado);
                }
            }
        }
        else
        {
            $empleados = $_empleados;
        }

        return $empleados;
    }

    public function detalle($id)
    {
        $data = file_get_contents("employees.json");
        $empleados = json_decode($data, true);

        foreach ($empleados as $empleado)
        {
            if ($empleado['id'] == $id)
            {
                return $empleado;
            }
        }
        return null;
    }

    public function empleadosPorRangoSueldo($rango1, $rango2)
    {
        $data = file_get_contents("employees.json");
        $_empleados = json_decode($data, true);
        $empleados = [];

        $mayor = ($rango1 >= $rango2) ? $rango1 : $rango2;
        $menor = ($rango2 < $rango1) ? $rango2 : $rango1;

        foreach ($_empleados as $empleado)
        {
            $salary = preg_replace("%[^0-9\\-\\. ]%","", $empleado['salary']);

            if ($salary <= $mayor && $salary >= $menor)
            {
                array_push($empleados, $empleado);
            }
        }
        return $empleados;
    }

    public function getXML($empleados)
    {
        header('Content-Type: text/xml');
        $xml = new \SimpleXMLElement('<empleados />');
        self::convertXML($empleados, $xml);
        print $xml->asXML();
    }

    public function convertXML($empleados, &$xml)
    {
        foreach ($empleados as $key => $value)
        {
            if (is_array($value))
            {
                if (is_numeric($key))
                {
                    $key = 'item' . $key;
                }
                $subnode = $xml->addChild($key);
                self::convertXML($value, $subnode);
            }
            else
            {
                $xml->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}