<?php

use Model\Empleado;

$app->get('/', function ($request, $response, $args) use ($app) {
    return $response->withRedirect($this->router->pathFor('empleados.index'));
})->setName("index");

$app->get('/empleados', function ($request, $response, $args) use ($app) {
    $objEmpleados = new Empleado();
    $empleados = $objEmpleados->getAll();
    $view = new Slim\Views\PhpRenderer("templates");
    $view->render($response, 'empleados.php', compact('empleados'));
})->setName('empleados.index');

$app->post('/empleados', function ($request, $response, $args) use ($app) {
    $req = $request->getParsedBody();
    $email = $req['email'];

    $objEmpleados = new Empleado();
    $empleados = $objEmpleados->buscar($email);

    $view = new Slim\Views\PhpRenderer("templates");
    $view->render($response, 'empleados.php', compact('empleados'));
});

$app->get('/empleado/{id}', function ($request, $response, $args) use ($app) {
    $objEmpleados = new Empleado();
    $empleado = $objEmpleados->detalle($args['id']);

    if (!is_null($empleado))
    {
        $view = new Slim\Views\PhpRenderer("templates");
        return $view->render($response, 'empleado.php', compact('empleado'));
    }

    return $response->withRedirect($this->router->pathFor('empleados.index'));
});

$app->get('/empleados-por-rango/{rango1}/{rango2}', function ($request, $response, $args) use ($app) {
    $objEmpleados = new Empleado();
    $empleados = $objEmpleados->empleadosPorRangoSueldo($args['rango1'], $args['rango2']);

    $objEmpleados->getXML($empleados);
});

