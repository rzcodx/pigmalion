<?php

class ChangeString
{
    protected $array_minusculas = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    protected $array_mayusculas = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

    public function build ($cad)
    {
        $nuevo_array = [];
        $nueva_cadena = "";
        $array_caracteres = str_split($cad);
        foreach ($array_caracteres as $caracter)
        {
            $array_temp = (ctype_upper($caracter)) ? $this->array_mayusculas : $this->array_minusculas;

            $indice = array_search($caracter, $array_temp);

            if ($indice === false)
            {
                $nueva_cadena .= $caracter;
            }
            else
            {
                $indice = ($indice == count($array_temp) - 1) ? 0 : $indice + 1;
                $nueva_cadena .= $array_temp[$indice];
            }
        }
        return $cad . " => " . $nueva_cadena;
    }
}

$prueba = new ChangeString();

print $prueba->build("123 abcd*3") . "<br />";
print $prueba->build("**Casa 52") . "<br />";
print $prueba->build("**Casa 52Z") . "<br />";
