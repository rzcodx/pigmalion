<?php

class CompleteRange
{
    public function build ($numeros)
    {
        $nuevo_array = [];
        $prev = $numeros[0];
        foreach ($numeros as $numero)
        {
            do
            {
                array_push($nuevo_array, $prev);
                $prev++;
            } while ($numero >= $prev);
        }
        return $nuevo_array;
    }
}

$prueba = new CompleteRange();

echo "<pre>";
var_dump($prueba->build([1, 2, 4, 5]));
var_dump($prueba->build([2, 4, 9]));
var_dump($prueba->build([55, 58, 60]));
echo "</pre>";