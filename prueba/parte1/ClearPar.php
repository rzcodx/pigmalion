<?php

class ClearPar
{
    public function build($cad)
    {
        $array_caracteres = str_split($cad);
        $nueva_cadena = "";
        $inicio = false;
        foreach ($array_caracteres as $caracter)
        {
            if ($caracter == "(")
            {
                $inicio = true;
            }
            if ($inicio and $caracter == ")")
            {
                $nueva_cadena .= "()";
                $inicio = false;
            }
        }
        return $cad . " => " . $nueva_cadena;
    }
}

$prueba = new ClearPar();

echo $prueba->build("()())()") . "<br />";
echo $prueba->build("()(()") . "<br />";
echo $prueba->build(")(") . "<br />";
echo $prueba->build("((()") . "<br />";
